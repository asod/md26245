Installation Scripts & Instructions For 26245
=============================================

https://md26245.rtfd.io/

Installs OpenMM and a bunch of tools on the students' accounts at the DTU HPC, and 
includes documentation on how to get through the installation process and end up 
running the Jupyter Notebook Exercises. 


Installation procedure *heavily* 'inspired' by the [GPAW summer school](https://wiki.fysik.dtu.dk/gpaw/summerschools/summerschool18/summerschool18.html) 
