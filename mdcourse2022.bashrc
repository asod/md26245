module load cuda/10.2
export OPENMM_CUDA_COMPILER=`which nvcc`
alias l="ls -ltr"
alias e="vim"
if  command -v conda &> /dev/null; then
    conda activate
fi
# history search with arrow keys - like Matlab
export HISTCONTROL=erasedups
export HISTSIZE=10000
shopt -s histappend
bind '"\e[A"':history-search-backward
bind '"\e[B"':history-search-forward
export PATH="$HOME/miniconda3/bin:$PATH"
export PATH="/zhome/c7/a/69784/MDCOURSE:$PATH"
